package com.jbehaveforjira.exampleproject.steps;

import com.jbehaveforjira.exampleproject.bookstore.Currency;
import com.jbehaveforjira.exampleproject.fxtradingapp.Cashflow;
import com.jbehaveforjira.exampleproject.fxtradingapp.FxTransaction;
import com.jbehaveforjira.exampleproject.fxtradingapp.MTMessageField;
import com.jbehaveforjira.exampleproject.fxtradingapp.SwiftMessageType;
import com.jbehaveforjira.javaclient.ComponentSteps;
import com.jbehaveforjira.javaclient.ParameterInfo;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Maryna Pristrom
 */
@ComponentSteps("Trading Application")
public class FXTradingAppSteps {

    private BigDecimal fxRate;

    private Currency buyCurrency;

    private BigDecimal amountToBuy;

    private Currency homeCurrency = Currency.EUR;

    private BigDecimal resultingAmount = new BigDecimal("0.00");

    @Given("FX rate of $rate to buy $buyCurrency with $sellCurrency")
    public void setFXRate(BigDecimal rate, Currency buyCurrency, Currency sellCurrency) {
        this.fxRate = rate;
        this.buyCurrency = buyCurrency;
        this.homeCurrency = sellCurrency;
    }

    @When("I execute an FX transaction to sell $amount of $currency")
    public void givenFxTransaction(
            @ParameterInfo(suggestedValues = {"100", "200", "300", "1000", "10000"},
                    formatPattern = "[0-9]{1,10}\\.[0-9]{2}",
                    formatDisplayText = "###.##") BigDecimal amount,
            Currency buyCurrency) {
        //
        this.resultingAmount = fxRate.multiply(amount);
    }


    @Then("I should have a total of $expectedTotal in my local currency")
    public void checkResultingAmount(BigDecimal expectedTotal) {

        Assert.assertTrue("Expected resulted amount of - " + expectedTotal + " but was - " + this.resultingAmount,
                this.resultingAmount.compareTo(expectedTotal) == 0);
    }

    @Given("the following FX transactions: $transactionFields")
    public void givenFxTransaction(List<FxTransaction> fxTransaction) {

    }

    @Then("those transactions should generate cashflows with the following fields: $expectedCashflows")
    public void checkCashflows(List<Cashflow> expectedCashflows) {

    }

    @Then("a new swift message of type $messageType should be generated with the following fields: $msgFields")
    public void checkSwiftMessage(SwiftMessageType messageType, List<MTMessageField> msgFields) {

    }

    @Then("the generated cashflow should be for value date $valueDate")
    public void checkValueDate(
            @ParameterInfo(formatPattern = "[1-3]?[0-9]/((0[1-9])|(1[0-2]))/[1,2][0-9]{3}", formatDisplayText = "dd/MM/yyyy")Date valueDate) {

    }

}

