package com.jbehaveforjira.exampleproject.steps;

import com.jbehaveforjira.javaclient.ComponentSteps;
import org.jbehave.core.annotations.*;
import org.jbehave.core.model.ExamplesTable;

/**
 * Created by Maryna Pristrom on 1/13/2016.
 */
@ComponentSteps("BDD")
public class ExampleBDDSteps {

    public static enum Outcome {
        successful, failed
    }

    @Given("a step with examples table $parameter")
    public void stepWithExamplesTable(ExamplesTable parameter) {

    }

    @Given("a step that always fails")
    public void failingStep() {
        throw new RuntimeException("This step is hardcoded to fail");
    }

    @Given("a step that is executed before each scenario")
    public void beforeScenario() {
    }

    @Given("a step that is executed after each scenario regardless of outcome")
    public void afterScenario() {
    }

    @Given("a step that is executed after each $outcome scenario")
    public void afterSuccessfulOrFailedScenario(Outcome outcome) {
    }

    public static enum StepType {
        occurrence, outcome
    }

    @Given("step represents a precondition to an event")
    public void preconditionMethod() {

    }

    @When("step represents the $stepType of the event")
    public void eventMethod(StepType stepType) {

    }

    @Then("step represents the $stepType of the event")
    public void eventMethod2(StepType stepType) {
        this.eventMethod(stepType);
    }

    public static enum Precondition {
        abc, xyz
    }

    public static enum EventType{
        positive, negative
    }

    @When ("a $eventType event occurs")
    public void givenEventType(EventType eventType) {

    }

    @Given("a stock of symbol $symbol and a threshold of $threshold") // standalone
//    @Alias("a stock of <symbol> and a <threshold>") // examples table
    public void aStock(@Named("symbol") String symbol, @Named("threshold") double threshold) {
        // ...
    }

    @When("the stock is traded at $price")
//    @Alias("the stock is traded at <price>") // examples table
    public void tradeStock(@Named("price") String price) {
        //...
    }

    @Then("the alert status should be $status")
//    @Alias("the alert status should be <status>") // examples table
    public void checkAlertStatus(@Named("status") String status) {
        //...
//        throw new RuntimeException("Hardcoded step failure");
    }


}
