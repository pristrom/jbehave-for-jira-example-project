package com.jbehaveforjira.exampleproject.steps;

import com.jbehaveforjira.exampleproject.atm.*;
import com.jbehaveforjira.exampleproject.util.StoryTestException;
import com.jbehaveforjira.javaclient.ComponentSteps;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Maryna Pristrom
 */
@ComponentSteps("ATM")
public class AtmSteps {

    private ATMAccountService atm;

    private WithdrawalRequest withdrawalRequest;
    private WithdrawalResponse withdrawalResponse;

    @BeforeScenario
    public void resetShoppingBasket() {
        atm = new ATMAccountService();
        withdrawalRequest = new WithdrawalRequest(new Account());
    }

    /**
     * Set the initial balance to the specified amount.
     * @param accountBalance - balance amount, decimal digits are allowed
     */
    @Given("the account balance is $accountBalance")
    public void givenAccBalance(double accountBalance) {

        this.withdrawalRequest.getAccount().setBalance(BigDecimal.valueOf(accountBalance));
    }

    /**
     * Sets the card status.
     * @param cardStatus - either 'valid' or 'disabled'
     */
    @Given("the card is $cardStatus")
    public void givenCardStatus(CardStatus cardStatus) {

        this.withdrawalRequest.getAccount().setCardStatus(cardStatus);
    }

    @Given("the machine contains enough money")
    public void setAtmCashToMax() {

        this.atm.setCashInAtm(new BigDecimal(Double.MAX_VALUE));
    }

    @Given("the machine does not contain enough money")
    public void setAtmCashToMin() {

        this.atm.setCashInAtm(new BigDecimal("0"));
    }

    @When("the machine does not contain enough money")
    public void whenAtmCashToMin() {

        this.atm.setCashInAtm(new BigDecimal("0"));
    }

    /**
     * Test javadoc on a When step method
     * @param amount
     */
    @When("the Account Holder requests $amount")
    public void withdrawAmount(double amount) {

        this.withdrawalRequest.setWithdrawAmount(new BigDecimal(amount));
        withdrawalResponse = this.atm.processRequest(this.withdrawalRequest);
    }

    @Then("the ATM should dispense $dispensedAmount")
    public void verifyWithdrawalWasSuccessful(double dispensedAmount) {

        // check withdrawal was successful
        boolean withdrawalWasSuccessful = this.withdrawalResponse.getMsgList().contains(WithdrawalResponse.MsgType.WITHDRAWAL_SUCCESSFUL);
        if (!withdrawalWasSuccessful) {
            throw new StoryTestException("Withdrawal was not successful");
        }

        BigDecimal lastDispensedAmt = this.atm.getLastDispensedAmt();
        BigDecimal expectedDispensedAmt = new BigDecimal(dispensedAmount);
        int equal = expectedDispensedAmt.compareTo(lastDispensedAmt);

        if (equal != 0) {
            throw new StoryTestException("Expected dispensed amount '" + expectedDispensedAmt + "'"
                    + " was different to actual dispensed amount '" + lastDispensedAmt + "'");
        } else {
            // verification check passed
        }
    }

    @Then ("the account balance should be $expectedBal")
    public void verifyAccBalance(double expectedBal) {

        BigDecimal expectedBalance = new BigDecimal(expectedBal);
        BigDecimal actualBalance = this.withdrawalRequest.getAccount().getBalance();

        int equal = expectedBalance.compareTo(actualBalance);

        if (equal != 0) {
            throw new StoryTestException("Expected account balance '" + expectedBalance + "'"
                    + " was different to actual account balance '" + actualBalance + "'");
        } else {
            // verification check passed
        }
    }

    public static enum AtmCardAction {
        returned, retained;
    }

    /**
     * Checks the card action.
     * @param atmCardAction - either 'returned' or 'retained'.
     */
    @Then ("the card should be $atmCardAction")
    public void verifyCardReturnedOrRetained(List<AtmCardAction> atmCardAction) {

        switch (atmCardAction.get(0)) {
            case returned:
                boolean wasReturned = this.withdrawalResponse.getMsgList().contains(WithdrawalResponse.MsgType.CARD_RETURNED);
                if (!wasReturned) {
                    throw new StoryTestException("Card was not returned by the ATM");
                }
                break;
            case retained:
                boolean wasRetained = this.withdrawalResponse.getMsgList().contains(WithdrawalResponse.MsgType.CARD_RETAINED);
                if (!wasRetained) {
                    throw new StoryTestException("Card was not retained by the ATM");
                }
                break;
        }
    }

    /**
     * Self explanatory.
     */
    @Then ("the ATM should not dispense any money")
    public void verifyWithdrawalFailed() {

        boolean withdrawalDeclined = this.withdrawalResponse.getMsgList().contains(WithdrawalResponse.MsgType.WITHDRAWAL_DECLINED);
        if (!withdrawalDeclined) {
            throw new StoryTestException("Withdrawal was completed successfully but was expected to be declined");
        }
    }

    @Then ("the ATM should say there are insufficient funds")
    public void verifyOutOfFundsMsgWasShown() {

        boolean outOfFundsShown = this.withdrawalResponse.getMsgList().contains(WithdrawalResponse.MsgType.INSUFFICIENT_FUNDS_IN_ACCOUNT);
        if (!outOfFundsShown) {
            throw new StoryTestException("ATM did not show insufficient funds message to the Account Holder");
        }
    }

    @Then ("the ATM should say the card has been retained")
    public void verifyCardRetainedMsgWasShown() {

        boolean cardRetainedMsgWasShown = this.withdrawalResponse.getMsgList().contains(WithdrawalResponse.MsgType.CARD_RETAINED_MSG_WAS_SHOWN);
        if (!cardRetainedMsgWasShown) {
            throw new StoryTestException("ATM did not show a message to the Account Holder that the card has been retained");
        }
    }

    @Then("the ATM should say that it is out of cash")
    public void verifyAtmOutOfCashMsgWasShown() {

        boolean atmOutOfCashWasShown = this.withdrawalResponse.getMsgList().contains(WithdrawalResponse.MsgType.ATM_OUT_OF_CASH);
        if (!atmOutOfCashWasShown) {
            throw new StoryTestException("ATM did not show a message to the Account Holder that it was out of cash");
        }
    }
}
