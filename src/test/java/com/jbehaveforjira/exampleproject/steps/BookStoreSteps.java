package com.jbehaveforjira.exampleproject.steps;

import com.jbehaveforjira.exampleproject.bookstore.Book;
import com.jbehaveforjira.exampleproject.bookstore.BookStore;
import com.jbehaveforjira.javaclient.ComponentSteps;
import org.jbehave.core.annotations.*;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Maryna Pristrom
 */
@ComponentSteps("Book Store")
public class BookStoreSteps {

    public static enum CheckoutOption {
        shown,
        hidden
    }


    private BookStore bookStore = new BookStore();

    private List<Book> shopingBasket; // simulates shopping basket


//    @Given("the following book: $book")
//    public void givenBook(Book book) {
//
//        bookStore.add(book);
//    }

    @BeforeScenario
    public void resetShoppingBasket() {
        shopingBasket = new ArrayList<Book>();
    }

    @Given("the following books in stock: $books")
    public void givenBooks(List<Book> books) {

        bookStore.add(books);
    }

    @Given("an empty shopping basket")
    public void givenEmptyShoppingBasket() {
        shopingBasket.clear();
    }

    @Alias("I add the following book to my shopping basket: $books")
    @When("I add the following books to my shopping basket: $books")
    public void addBooksToShoppingBasket(List<Book> books) {
        shopingBasket.addAll(books);
    }

    @Then("sum total of my basket should be $sumTotal")
    public void checkBasketTotal(BigDecimal expectedTotal) {

        BigDecimal total = new BigDecimal("0.00");

        for (Book book : shopingBasket) {
            BigDecimal price = book.getPrice();
            total = total.add(price);
        }

        Assert.assertTrue("Sum total was - " + total + " but expected - " + expectedTotal, total.compareTo(expectedTotal) == 0);
    }

    @Then("checkout option should be $checkoutOption")
    public void verifyCheckoutOption(CheckoutOption checkoutOption) {

        if (shopingBasket.isEmpty()) {
            Assert.assertEquals(CheckoutOption.hidden, checkoutOption);
        } else {
            Assert.assertEquals(CheckoutOption.shown, checkoutOption);
        }
    }

}
