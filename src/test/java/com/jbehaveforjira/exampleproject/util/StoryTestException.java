package com.jbehaveforjira.exampleproject.util;

/**
 * @author Maryna Pristrom
 */
public class StoryTestException extends RuntimeException {

    public StoryTestException(String message, Throwable cause) {
        super(message, cause);
    }

    public StoryTestException(String message) {
        super(message);
    }
}
