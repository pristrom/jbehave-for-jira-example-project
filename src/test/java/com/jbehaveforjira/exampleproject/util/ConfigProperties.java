package com.jbehaveforjira.exampleproject.util;

import com.jbehaveforjira.exampleproject.TestRunner;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum ConfigProperties {

    url("jira.url"),
    username("jira.username"),
    password("jira.password"),

    project("jira.project"),
    environment("test.environment"),

    consumerKey("jira.consumerKey"),
    consumerSecret("jira.consumerSecret"),
    accessToken("jira.accessToken"),
    privateKey("jira.privateKey"),
    privateKeyFile("jira.privateKeyFile");

    private final String propertyKey;

    private static Properties properties;

    ConfigProperties(String propertyKey) {
        this.propertyKey = propertyKey;
    }

    public static void init() {

        String configFile;

        String instanceName = System.getProperty("instance");
        if (instanceName != null && !instanceName.isEmpty()) {
            configFile = instanceName.trim();
        } else {
            configFile = "localhost";
        }
        configFile += ".properties";

        properties = new Properties();
        try (InputStream is = TestRunner.class.getClassLoader().getResourceAsStream(configFile)) {
            properties.load(is);
        } catch (IOException e) {
            throw new IllegalArgumentException("Failed to load configuration properties file using path - " + configFile, e);
        }

        validate();
    }

    private static void validate() {

        validateNotEmpty(url, project, environment);

        if (accessToken.get() != null) {
            validateNotEmptyRelatedProperties(accessToken, consumerKey, consumerSecret);
            String privateKey = ConfigProperties.privateKey.get();
            if (privateKey == null || privateKey.trim().isEmpty()) {
                String privateKeyFile = ConfigProperties.privateKeyFile.get();
                if (privateKeyFile == null || privateKeyFile.trim().isEmpty()) {
                    throw new IllegalArgumentException("While setting a value for '" + accessToken + "' you must also set value for either '" + ConfigProperties.privateKey + "' or '" + ConfigProperties.privateKeyFile + "'");
                }
            }
        } else {
            validateNotEmpty(username, password);
        }
    }

    private static void validateNotEmpty(ConfigProperties... properties) {
        for (ConfigProperties property : properties) {
            Validate.notBlank(property.get(), "Configuration property '" + property + "' cannot be empty");
        }
    }

    private static void validateNotEmptyRelatedProperties(ConfigProperties dependentProperty, ConfigProperties... properties) {
        for (ConfigProperties property : properties) {
            Validate.notBlank(property.get(), "While setting a value for the '" + dependentProperty + "' you must also set value for '" + property + "'");
        }
    }

    public String get() {
        String result = (String) properties.get(propertyKey);
        if (result != null && !result.trim().isEmpty()) {
            return result.trim();
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return propertyKey;
    }
}
