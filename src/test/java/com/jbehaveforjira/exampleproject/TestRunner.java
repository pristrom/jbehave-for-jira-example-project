package com.jbehaveforjira.exampleproject;


import com.jbehaveforjira.exampleproject.steps.AtmSteps;
import com.jbehaveforjira.exampleproject.steps.BookStoreSteps;
import com.jbehaveforjira.exampleproject.steps.ExampleBDDSteps;
import com.jbehaveforjira.exampleproject.steps.FXTradingAppSteps;
import com.jbehaveforjira.exampleproject.util.ConfigProperties;
import com.jbehaveforjira.javaclient.*;
import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporter;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.CandidateSteps;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.jbehaveforjira.exampleproject.util.ConfigProperties.*;

/**
 * @author Maryna Pristrom
 */
public class TestRunner extends JUnitStories {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @BeforeClass
    public static void init() {

        ConfigProperties.init();
    }

    public TestRunner() {

        MostUsefulConfiguration configuration = new MostUsefulConfiguration();

        // set custom Jira HTML output format
        configuration.useStoryReporterBuilder(
                new StoryReporterBuilder() {
                    public StoryReporter reporterFor(String storyPath, org.jbehave.core.reporters.Format format) {
                        if (format.equals(org.jbehave.core.reporters.Format.HTML_TEMPLATE)) {

                            Keywords keywords = keywords();
                            JiraStoryReporter jiraReporter = new JiraStoryReporter(url.get(), environment.get(), keywords)
                                    //.withOauthParams(consumerKey.get(), consumerSecret.get(), accessToken.get(), new File(privateKeyFile.get()));
                                    .withUsernamePassword(username.get(), password.get())
                                    .withRequestTimeout(20, TimeUnit.SECONDS)
                                    .doReportToDir(new File("target/jbehave_for_jira/reports"));

                            return jiraReporter;

                        } else {
                            return super.reporterFor(storyPath, format);
                        }
                    }
                }
                        .withFailureTrace(true)
                        .withFormats(
                                Format.CONSOLE,
                                Format.HTML,
                                Format.HTML_TEMPLATE,
                                Format.STATS
                        )
        );

        // set Jira story loader
        JiraStoryLoader jiraStoryLoader = new JiraStoryLoader(url.get())
                //.withOauthParams(consumerKey.get(), consumerSecret.get(), accessToken.get(), new File(privateKeyFile.get()));
                .withUsernamePassword(username.get(), password.get())
                .doPrependIssueTitleToStory(true)
                .withRequestTimeout(20, TimeUnit.SECONDS)
                .downloadStoriesIntoDir("target/jbehave_for_jira/stories");

        configuration.useStoryLoader(jiraStoryLoader);

        // set Jira step doc reporter
        JiraStepDocReporter stepDocReporter = new JiraStepDocReporter(url.get(), project.get())
                //.withOauthParams(consumerKey.get(), consumerSecret.get(), accessToken.get(), new File(privateKeyFile.get()));
                .withUsernamePassword(username.get(), password.get())
                .withRequestTimeout(20, TimeUnit.SECONDS)
                .doIncludeStepJavaDocs();
        configuration.useStepdocReporter(stepDocReporter);

        useConfiguration(configuration);
    }

    @Test
    public void run() {

        Embedder embedder = configuredEmbedder();

        embedder.embedderControls()
                .doIgnoreFailureInStories(false)
                .doIgnoreFailureInView(true)
                .useStoryTimeouts("" + Long.MAX_VALUE);

        List<String> filters = embedder.metaFilters();
        filters.add("-skip");
        embedder.useMetaFilters(filters);

        try {
            embedder.runStoriesAsPaths(storyPaths());
        } finally {
            // report step docs back to jira even if there is a failure during story execution
            try {
                List<CandidateSteps> candidateSteps = embedder.stepsFactory().createCandidateSteps();
                embedder.reportStepdocs(configuration(), candidateSteps);
            } catch (RuntimeException e) {
                logger.error("An error has occurred while attempting to publish step doc information to jira", e);
            }
        }

    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(),
                new AtmSteps(),
                new BookStoreSteps(),
                new FXTradingAppSteps(),
                new ExampleBDDSteps());
    }

    @Override
    protected List<String> storyPaths() {

        JiraStoryPathsFinder storyFinder = new JiraStoryPathsFinder(url.get(), project.get())
                //storyFinder.withOauthParams(consumerKey.get(), consumerSecret.get(), accessToken.get(), new File(privateKeyFile.get()));
                .withUsernamePassword(username.get(), password.get())
                .withRequestTimeout(20, TimeUnit.SECONDS);

        // set additional filtering parameters here if required
//        storyFinder.withIssueFieldFilterParam(JiraStoryPathsFinder.IssueFilterParam.TYPE, "Story");
//        storyFinder.withIssueFieldFilterParam(JiraStoryPathsFinder.IssueFilterParam.LABELS, "test|demo");

        List<String> paths = storyFinder.findPaths();
        return paths;
    }
}
