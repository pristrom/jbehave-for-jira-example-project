example story specifying a simple FX deal

Narrative:
In order to make sure that I don't loose any money
As a FX trader
I want to verify that the FX rates are applied correctly

Scenario: simple FX deal
Given FX rate of 0.79 to buy GBP with EUR
When I execute an FX transaction to sell 100 of GBP
Then I should have a total of 126.58 in my local currency

Scenario: check correct cashflows are generated from an FX deal
Given the following FX transactions:
|Trader name|Counterparty  |FX Type|Buy/Sell|Buy Currency|Sell Currency|rate   |Amount|Sett. Date|
|Alison     |Counterparty A|SPOT   |Buy     |USD         |EUR          |1.4    |1000  |today+2   |
|Bob        |Counterparty B|FORWARD|Sell    |TZS         |GBP          |2733.40|10    |today+5   |

Then those transactions should generate cashflows with the following fields:
|Sett. system|Currency|Payment date|Pay/Receive|Amount|Int. account|Counterparty|Ext. account|
|SWIFT       |USD     |today+2     |RECEIVE    |1400  |123456      |Big Bank A  |987654      |
|SWIFT       |EUR     |today+2     |PAY        |1000  |222333      |Big Bank A  |999888      |
And a new swift message of type MT300 should be generated with the following fields:
|messageTag|tagValue        |
|22A       |NEW             |
|22C       |EDA5330218IBBA33|
|36        |1.40            |
|82A       |my bank         |
|87A       |Counterparty A  |
|32B       |EUR             |
