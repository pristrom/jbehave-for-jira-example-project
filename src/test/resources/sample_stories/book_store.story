example story specifying Book Store behaviour

Narrative:
In order to make sure that I'm charged a correct amount
As a book store customer
I want to verify that the basket total is calculated correctly

Scenario: adding a book
Given an empty shopping basket
And the following books in stock:
|Title             |Genre    |Author            |Price|ISBN         |Year pub.|In stock|Format   |
|1984              |Classics |George Orwell     |12.99|0451524352934|1950     |true    |Paperback|
|Then Final Problem|Detective|Arthur Conan Doyle|5.99 |1480067812434|2012     |true    |Hardcover|
When I add the following book to my shopping basket:
|Title            |Genre    |Author            |Price|
|The Final Problem|Detective|Arthur Conan Doyle|5.99 |
Then sum total of my basket should be 5.99
