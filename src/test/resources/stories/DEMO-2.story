example story specifying Book Store behavior

Narrative:
In order to make sure that I'm charged the correct amount
As a book store customer
I want to verify that the basket total is calculated correctly

Scenario: another scenario
Given the following books:
|Title             |Genre    |Author            |price|ISBN      |Year pub.|inStock|Format   |
|1984              |Classics |George Orwell     |12.99|0451524934|1950     |true   |Paperback|
|Then Final Problem|Detective|Arthur Conan Doyle|5.99 |1480067814|2012     |true   |Hardcover|
When I add the following books to my shopping basket:
|Title            |Genre    |Author            |price|
|The Final Problem|Detective|Arthur Conan Doyle|5.99 |
Then sum total of my basket should be 5.99 

Scenario: saved from plugin..