package com.jbehaveforjira.exampleproject.fxtradingapp;

public enum SwiftMessageType {
    MT300, MT305, MT310, MT101, MT102, MT900, MT910, MT950
}
