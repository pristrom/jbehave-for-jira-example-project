package com.jbehaveforjira.exampleproject.fxtradingapp;

import com.jbehaveforjira.exampleproject.bookstore.Currency;
import com.jbehaveforjira.javaclient.ParameterInfo;
import org.jbehave.core.annotations.AsParameters;
import org.jbehave.core.annotations.Parameter;

import java.util.Date;

@AsParameters
public class FxTransaction {

    @Parameter(name = "Trader name")
    @ParameterInfo(suggestedValues = {"Andrew", "George", "Susan", "Helen"})
    String traderName;

    @Parameter(name = "Counterparty")
    String counterpartyName;

    @Parameter(name = "Counterparty BIC")
    String counterpartyBic;

    @Parameter(name = "FX Type")
    FXType fxType;

    @Parameter(name = "Buy/Sell")
    BuySellIndicator buyOrSellIndicator;

    @Parameter(name = "Buy Currency")
    @ParameterInfo(isMandatory = true)
    Currency buyCurrency;

    @Parameter(name = "Sell Currency")
    @ParameterInfo(isMandatory = true)
    Currency sellCurrency;

    @Parameter(name = "rate")
    @ParameterInfo(isMandatory = true)
    Double rate;

    @Parameter(name = "Amount")
    @ParameterInfo(isMandatory = true)
    Double amount;

    @Parameter(name = "Tx. Date")
    Date transactionDate;

    @Parameter(name = "Sett. Date")
    Date settlementDate;

    @Parameter(name = "Account")
    String account;

    @Parameter(name = "Reference")
    Long reference;

    @Parameter(name = "Value date")
    @ParameterInfo(formatPattern = "[1-3]?[0-9]/((0[1-9])|(1[0-2]))/[1,2][0-9]{3}", formatDisplayText = "dd/MM/yyyy")
    String valueDate;
}
