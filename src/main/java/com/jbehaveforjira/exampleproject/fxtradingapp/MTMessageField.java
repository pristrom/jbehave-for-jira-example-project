package com.jbehaveforjira.exampleproject.fxtradingapp;

import com.jbehaveforjira.javaclient.utils.StepParameter;
import org.jbehave.core.annotations.AsParameters;

@AsParameters
public class MTMessageField {

    public static enum MessageTag implements StepParameter {

        _15A, _20, _22A, _22C, _82A, _87A,
        _15B, _30T, _30V, _36, _32B, _33B, _57A,
        _15C, _24D;

        public String toString() {

            return this.name().substring(1);
        }

        @Override
        public String asString() {
            return this.name().substring(1);
        }

        @Override
        public void fromString(String asString) {
            String temp = "_" + asString;
            MessageTag.valueOf(temp);
        }
    }

    private MessageTag messageTag;

    private String tagValue;

    public MessageTag getMessageTag() {
        return messageTag;
    }

    public String getTagValue() {
        return tagValue;
    }
}
