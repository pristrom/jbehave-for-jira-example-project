package com.jbehaveforjira.exampleproject.fxtradingapp;

public enum FXType {
    SPOT,
    FORWARD,
    SWAP

}
