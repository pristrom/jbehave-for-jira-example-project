package com.jbehaveforjira.exampleproject.fxtradingapp;

public enum BuySellIndicator {
    Buy,
    Sell
}
