package com.jbehaveforjira.exampleproject.fxtradingapp;

import com.jbehaveforjira.exampleproject.bookstore.Currency;
import org.jbehave.core.annotations.AsParameters;
import org.jbehave.core.annotations.Parameter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Maryna Pristrom
 */
@AsParameters
public class Cashflow {

    public static enum PaymentType {
        principal, coupon, brokerage, clearing
    }

    public static enum SettlementSystem {
        SWIFT, CHAPS, FEDWIRE, TARGET2
    }

    public static enum TradeType {
        NEW, REPLACE, CANCEL
    }

    public static enum Direction {
        PAY, RECEIVE
    }

    @Parameter(name = "Payment type")
    private PaymentType paymentType;

    @Parameter(name = "Sett. system")
    private SettlementSystem settlementSystem;

    @Parameter(name = "Currency")
    private Currency currency;

    @Parameter(name = "Payment date")
    private Date paymentDate;

    @Parameter(name = "Pay/Receive")
    private Direction direction;

    @Parameter(name = "Amount")
    private BigDecimal amount;

    @Parameter(name = "Int. account")
    private String ownAccount;

    @Parameter(name = "Counterparty")
    private String counterparty;

    @Parameter(name = "Ext. account")
    private String counterpartyAccount;

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public SettlementSystem getSettlementSystem() {
        return settlementSystem;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public Direction getDirection() {
        return direction;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getOwnAccount() {
        return ownAccount;
    }

    public String getCounterparty() {
        return counterparty;
    }

    public String getCounterpartyAccount() {
        return counterpartyAccount;
    }
}
