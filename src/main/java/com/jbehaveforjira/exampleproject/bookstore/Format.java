package com.jbehaveforjira.exampleproject.bookstore;

/**
 * @author Maryna Pristrom
 */
public enum Format {

    Hardcover,
    Paperback,
    Audible,
    eBook
}
