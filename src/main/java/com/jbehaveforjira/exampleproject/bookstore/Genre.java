package com.jbehaveforjira.exampleproject.bookstore;

/**
 * @author Maryna Pristrom
 */
public enum Genre {
    Fiction,
    Drama,
    Detective,
    Romance,
    Mystery,
    Fantasy,
    Classics
}
