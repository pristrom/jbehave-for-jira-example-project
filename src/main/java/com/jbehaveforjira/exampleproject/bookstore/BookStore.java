package com.jbehaveforjira.exampleproject.bookstore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Maryna Pristrom
 */
public class BookStore {

    private List<Book> books = new ArrayList<Book>();

    public void add(Collection<Book> books) {
        this.books.addAll(books);
    }

    public void add(Book book) {
        this.books.add(book);
    }

    public boolean remove(Book book) {
        return this.books.remove(book);
    }

    public void removeAll() {
        books.clear();
    }

    public List<Book> findAll() {
        return books;
    }

    public List<Book> findAllByGenre(Genre genre) {
        List<Book> results = new ArrayList<Book>();
        for (Book book : books) {
            if (book.getGenre().equals(genre)) {
                results.add(book);
            }
        }
        return results;
    }

}
