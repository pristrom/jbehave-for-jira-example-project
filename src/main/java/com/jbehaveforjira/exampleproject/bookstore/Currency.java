package com.jbehaveforjira.exampleproject.bookstore;

public enum Currency {

    USD,
    EUR,
    GBP,
    TZS
}
