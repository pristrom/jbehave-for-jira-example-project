package com.jbehaveforjira.exampleproject.bookstore;

import com.jbehaveforjira.javaclient.ParameterInfo;
import org.jbehave.core.annotations.AsParameters;
import org.jbehave.core.annotations.Parameter;

import java.math.BigDecimal;

/**
 * @author Maryna Pristrom
 */
@AsParameters
public class Book {

    @ParameterInfo(isMandatory = true)
    @Parameter(name = "Title")
    private String title;

    @Parameter(name = "Genre")
    private Genre genre;

    @ParameterInfo(isMandatory = true)
    @Parameter(name = "Author")
    private String author;

    @ParameterInfo(suggestedValues = {"1.99", "9.93", "12.50"})
    @Parameter(name = "Price")
    private BigDecimal price;

    @Parameter(name = "Discount")
    @ParameterInfo(allowedValues = {"10", "15", "30"})
    private Double discount;

    @ParameterInfo(formatPattern = "[0-9]{13}", formatDisplayText = "13 digit number")
    private Integer ISBN;

    @ParameterInfo(suggestedValues = {"1990", "1991", "1992", "1993", "1994", "1995"})
    @Parameter(name = "Year pub.")
    private Integer yearPublished;

    @Parameter(name = "In stock")
    private Boolean inStock;

    @Parameter(name = "Format")
    @ParameterInfo(suggestedValuesEnum = Format.class)
    private String format;

    public Double getDiscount() {
        return discount;
    }

    public Integer getYearPublished() {
        return yearPublished;
    }

    public String getTitle() {
        return title;
    }

    public Genre getGenre() {
        return genre;
    }

    public String getAuthor() {
        return author;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Integer getISBN() {
        return ISBN;
    }

    public Boolean getInStock() {
        return inStock;
    }

    public String getFormat() {
        return format;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (author != null ? !author.equals(book.author) : book.author != null) return false;
        if (genre != book.genre) return false;
        if (title != null ? !title.equals(book.title) : book.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", genre=" + genre +
                ", author='" + author + '\'' +
                '}';
    }
}
