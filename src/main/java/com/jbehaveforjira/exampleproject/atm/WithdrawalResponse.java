package com.jbehaveforjira.exampleproject.atm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maryna Pristrom
 */
public class WithdrawalResponse {

    public static enum MsgType {
        WITHDRAWAL_SUCCESSFUL,
        WITHDRAWAL_DECLINED,
        INSUFFICIENT_FUNDS_IN_ACCOUNT,
        WITHDRAWAL_FAILED,
        ATM_OUT_OF_CASH,
        CARD_RETURNED,
        CARD_RETAINED,
        CARD_RETAINED_MSG_WAS_SHOWN
    }

    private List<MsgType> msgList = new ArrayList<MsgType>();

    public List<MsgType> getMsgList() {
        return msgList;
    }
}
