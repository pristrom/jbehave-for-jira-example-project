package com.jbehaveforjira.exampleproject.atm;

import java.math.BigDecimal;

/**
 * @author Maryna Pristrom
 */
public class Account {

    private BigDecimal balance = new BigDecimal("0.00");

    private CardStatus cardStatus = CardStatus.valid;

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public CardStatus getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(CardStatus cardStatus) {
        this.cardStatus = cardStatus;
    }
}
