package com.jbehaveforjira.exampleproject.atm;

import java.math.BigDecimal;

/**
 * @author Maryna Pristrom
 */
public class WithdrawalRequest {

    private final Account account;

    private BigDecimal withdrawAmount;

    public BigDecimal getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(BigDecimal withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public WithdrawalRequest(Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

}
