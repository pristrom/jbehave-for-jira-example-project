package com.jbehaveforjira.exampleproject.atm;

import java.math.BigDecimal;

/**
 * @author Maryna Pristrom
 */
public class ATMAccountService {

    private BigDecimal cashInAtm;

    private BigDecimal lastDispensedAmt;

    public BigDecimal getCashInAtm() {
        return cashInAtm;
    }

    public void setCashInAtm(BigDecimal cashInAtm) {
        this.cashInAtm = cashInAtm;
    }

    public BigDecimal getLastDispensedAmt() {
        return lastDispensedAmt;
    }

    public void setLastDispensedAmt(BigDecimal lastDispensedAmt) {
        this.lastDispensedAmt = lastDispensedAmt;
    }

    public WithdrawalResponse processRequest(WithdrawalRequest withdrawalRequest) {

        Account account = withdrawalRequest.getAccount();
        CardStatus cardStatus = account.getCardStatus();

        WithdrawalResponse response = new WithdrawalResponse();
        if (cardStatus == CardStatus.disabled) {
            response.getMsgList().add(WithdrawalResponse.MsgType.WITHDRAWAL_DECLINED);
            response.getMsgList().add(WithdrawalResponse.MsgType.CARD_RETAINED);
        } else {
            BigDecimal withdrawAmount = withdrawalRequest.getWithdrawAmount();
            BigDecimal newBalance = account.getBalance().subtract(withdrawAmount);
            if (newBalance.compareTo(new BigDecimal("0")) < 0) {
                response.getMsgList().add(WithdrawalResponse.MsgType.WITHDRAWAL_DECLINED);
                response.getMsgList().add(WithdrawalResponse.MsgType.INSUFFICIENT_FUNDS_IN_ACCOUNT);
                response.getMsgList().add(WithdrawalResponse.MsgType.CARD_RETURNED);
            } else {
                this.lastDispensedAmt = withdrawAmount;
                withdrawalRequest.getAccount().setBalance(newBalance);
                response.getMsgList().add(WithdrawalResponse.MsgType.WITHDRAWAL_SUCCESSFUL);
                response.getMsgList().add(WithdrawalResponse.MsgType.CARD_RETURNED);
            }
        }
        return response;
    }
}
