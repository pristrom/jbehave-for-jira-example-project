package com.jbehaveforjira.exampleproject.atm;

/**
* Created by Dmytro on 6/28/2015.
*/
public enum CardStatus {
    valid, disabled
}
